api = 2
core = 7.x


; Modules
projects[ctools][version] = "1.5"
projects[color_field][version] = "1.6"
projects[cors][version] = "1.3"
projects[geolocation][version] = "1.6"
projects[l10n_update][version] = "1.1"
projects[libraries][version] = "2.2"
projects[term_reference_tree][version] = "1.10"
projects[views][version] = "3.8"
projects[views_datasource][version] = "1.x-dev"

; Libraries
libraries[donde][download][type] = "file"
libraries[donde][download][url] = "https://github.com/mariano-dagostino/donde/archive/master.zip"
libraries[donde][directory_name] = "donde"
libraries[donde][type] = "library"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

