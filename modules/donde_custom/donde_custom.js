/**
 * @file
 * Attaches the behaviors for the Donde Distribution.
 */

(function ($) {
Donde.appStarted = function() {
  Donde.Instances.CategoryCollection.each(function(c){
    var category = c.toJSON();
    $('#category-' + category.id).parent().find('label').css({'border-bottom': category.color + ' solid 4px'});
  });
}

Donde.markerClick = function(e) {
  $.colorbox({href: 'donde/place/' + e.id, width: '700px', height: '500px'});
}

Drupal.behaviors.donde = {
  attach: function(context, settings) {
    Donde.Settings.BackendURL = {
      Places: 'donde/places',
      Categories: 'donde/categories',
      Relations: 'donde/relations'
    };

    Donde.Settings.Map.lat = settings.donde_custom.lat;
    Donde.Settings.Map.lng =  settings.donde_custom.lng;
    Donde.Settings.Map.zoom = 12;
    Donde.Settings.MarkersPath = settings.donde_custom.image_path;

    Donde.start();
  }
};

})(jQuery);


